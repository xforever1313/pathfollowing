using UnityEngine;

public class Flocker : SteeringVehicle{
	
	protected override void Update(){
		return;
	}
	
	protected override Vector3 calculateCustomSteeringForces(){
		Vector3 force = Vector3.zero;
		
		force += (attr.seekWt * steer.follow (gameManager.getPath(), Target));
		
		force += (attr.separationWt * steer.Separation(gameManager.getDistancesBetweenGuysArray(ID), gameManager.getGuys()));
		
		return force;
	}
	
	void LateUpdate()
	{	
		base.Update();
	}
}