﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path {
	
	private List<WayPoint> wayPoints;
	
	public Path(List<WayPoint> wayPoints){
		this.wayPoints = wayPoints;
	}
	
	public WayPoint getWayPointAtIndex(int i){
	    return wayPoints[i];	
	}
	
	public int getNumberOfWayPoints(){
	    return wayPoints.Count;	
	}
	
	public int getNextWayPointIndex(int index){
		int ret;
		if (index == (wayPoints.Count - 1)){
		    ret = 0;	
		}
		else{
		    ret = index + 1;
		}
		return ret;
	}
	
	public int getPreviousWayPointIndex(int index){
	    int ret;
		if (index == 0){
		    ret = wayPoints.Count - 1;	
		}
		else{
		    ret = index - 1;	
		}
		return ret;
	}
}
