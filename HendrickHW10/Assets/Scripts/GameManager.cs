﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
	
	public int NUMBER_OF_WAYPOINTS = 24;
	public int NUMBER_OF_FLOCKERS = 5;
	
	public GameObject guyPrefab;
	
	Path path;
	
	List<GameObject> guys;
	
	int guyWhoHasCamera;
	
	float [][] guyDistances;
	
	public Camera overheadCamera;
	private Camera mainCamera;
	
	public GUIText flockerText;
	
	// Use this for initialization
	void Start () {
		List<WayPoint> wayPoints = new List<WayPoint>();
		for (int i = 0; i < NUMBER_OF_WAYPOINTS; ++i){
			GameObject go = GameObject.Find("WP" + i);
			wayPoints.Add(new WayPoint(go, i));

			if (i != 0){
			   	wayPoints[i-1].setNextWaypoint(wayPoints[i]);
			}
		}
		wayPoints[wayPoints.Count-1].setNextWaypoint(wayPoints[0]);
		
		path = new Path(wayPoints);
		
		guys = new List<GameObject>();
		for (int i = 0; i < NUMBER_OF_FLOCKERS; ++i){
			float x = Random.Range(650, 950);
			float z = Random.Range (200, 825);
			float y = Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z));
			GameObject temp = (GameObject)GameObject.Instantiate(guyPrefab, new Vector3(x, y + 2, z), Quaternion.identity);
			guys.Add (temp);
		}
		
		//Allocate space for flocker distances
		guyDistances = new float [guys.Count][];
		for (int i = 0; i < guys.Count; ++i){
			guyDistances[i] = new float[guys.Count];
		}
		
		guyWhoHasCamera = 0;
		overheadCamera.enabled = false;
		Camera.main.GetComponent<SmoothFollow>().target = guys[guyWhoHasCamera].transform;
		mainCamera = Camera.main;
	}
	
	private void updateFlockerDistance(){
		for (int i = 0; i < guys.Count; ++i){
			for (int j = i; j < guys.Count; ++j){
				if (i == j){
					guyDistances[i][j] = 0;
				}
				else{
			        guyDistances[i][j] = Vector3.Distance(guys[i].transform.position, guys[j].transform.position);
				    guyDistances[j][i] = guyDistances[i][j];
				}
			}
		}
	}
	
	//For debugging only
	private void testGuyDistances(){
        for (int i = 0; i < guys.Count; ++i){
		    string row = "";
		    for (int j = 0; j < guys.Count; ++j){
			   row += guyDistances [i] [j] + " ";
			}
			Debug.Log(row);
		}
		throw new UnityException("DONE");
	}
	
	// Update is called once per frame
	void Update () {
		if (!overheadCamera.enabled){
			if (Input.GetKeyDown(KeyCode.RightArrow)){
				incrementCamera ();
			}
			else if (Input.GetKeyDown(KeyCode.LeftArrow)){
				decrementCamera();
			}
			else if (Input.GetKeyDown(KeyCode.Space)){
			    swapCamera();	
			}
			flockerText.enabled = true;
			flockerText.text = "Guy ID: " + guyWhoHasCamera;
		}
		else if (Input.GetKeyDown (KeyCode.Space)){
		    swapCamera();	
		}
		else if (overheadCamera.enabled){
			flockerText.text = "";
		}
		
		updateFlockerDistance();
		//testGuyDistances();
	}
	
	public Path getPath(){
	    return path;	
	}
	
	private void swapCamera(){
	    mainCamera.enabled = !mainCamera.enabled;
		overheadCamera.enabled = !overheadCamera.enabled;
	}
	
	private void incrementCamera(){
	    ++guyWhoHasCamera;
		if (guyWhoHasCamera == guys.Count){
		    guyWhoHasCamera = 0;	
		}
		
		mainCamera.GetComponent<SmoothFollow>().target = guys[guyWhoHasCamera].transform;
	}
	
    private void decrementCamera(){
		--guyWhoHasCamera;
		if (guyWhoHasCamera < 0){
		    guyWhoHasCamera = guys.Count - 1;	
		}
		mainCamera.GetComponent<SmoothFollow>().target = guys[guyWhoHasCamera].transform;
	}
	
	public float getDistancesBetweenGuys(int guyID1, int guyID2){
	    return guyDistances[guyID1] [guyID2];	
	}
	
	public float [] getDistancesBetweenGuysArray(int guyID){
	    return guyDistances[guyID];	
	}
	
	public List<GameObject> getGuys(){
	    return guys;	
	}
}
