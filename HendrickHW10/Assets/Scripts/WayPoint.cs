﻿using UnityEngine;
using System.Collections;

public class WayPoint {
	
	private GameObject go;
	
	public readonly int ID;
	
	WayPoint nextWayPoint;
	
	private float distanceToNextWayPoint;
	private Vector3 vectorToNextWayPoint;
	private Vector3 normalizedVectorToNextWayPoint;
	
	public WayPoint(GameObject go, int id){
		this.go = go;
		ID = id;
		distanceToNextWayPoint = 0;
		nextWayPoint = null;
	}
	
	public bool isNearWayPoint(Vector3 v, float radius){
		bool ret = false;
	    if (Vector3.Distance(v, go.transform.position) < radius){
		    ret = true;	
		}
		return ret;
	}
	
	public Vector3 getPosition(){
	    return go.transform.position;	
	}
	
	public void setNextWaypoint(WayPoint other){
		nextWayPoint = other;
		distanceToNextWayPoint = Vector3.Distance(go.transform.position, other.go.transform.position);
		vectorToNextWayPoint = other.go.transform.position - go.transform.position;
		normalizedVectorToNextWayPoint = vectorToNextWayPoint.normalized;
	}
	
	public float getDistanceToNextWayPoint(){
	    return distanceToNextWayPoint;	
	}
	
	public Vector3 getVectorToNextWayPoint(){
	    return vectorToNextWayPoint;	
	}
	
	public Vector3 getNormalizedVectorToNextWayPoint(){
	    return normalizedVectorToNextWayPoint;	
	}
	
	public WayPoint getNextWayPoint(){
	    return nextWayPoint;	
	}
}
