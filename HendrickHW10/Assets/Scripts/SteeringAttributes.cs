﻿using UnityEngine;
using System.Collections;

public class SteeringAttributes : MonoBehaviour {
	
	//these are common attributes required for steering calculations 
	public float maxForce = 30.0f;
	public float maxSpeed = 100.0f;
	public float mass = 1.0f;
	public float radius = 5f;
	
// These weights will be exposed in the Inspector window

	public float seekWt = 70.0f;
    public float separationWt = 30.0f;
	public float separationDist = 15.0f;
	public float pathRadius = 10f;
}
