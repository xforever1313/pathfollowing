﻿// The Steer component has a collection of functions
// that return forces for steering 

using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(CharacterController))]

public class Steer : MonoBehaviour
{
	Vector3 dv = Vector3.zero; 	// desired velocity, used in calculations
	SteeringAttributes attr; 	// attr holds several variables needed for steering calculations
	CharacterController body;
	
	void Start ()
	{
		GameObject main = GameObject.Find("MainGO");
		attr = main.GetComponent<SteeringAttributes> ();
		body = gameObject.GetComponent<CharacterController> ();	
	}
	
	
	//-------- functions that return steering forces -------------//
	public Vector3 Flee (Vector3 targetPos)
	{
		//find dv, desired velocity
		dv = transform.position - targetPos;		
		dv = dv.normalized * attr.maxSpeed; 	//scale by maxSpeed
		dv -= body.velocity;
		return dv;
	}
	
	public Vector3 Seek(Vector3 targetPos){
		//find dv, desired velocity
		dv = targetPos - transform.position;		
		dv = dv.normalized * attr.maxSpeed; 	//scale by maxSpeed
		dv -= body.velocity;
		return dv;
	}
	
	//These functions will be called from calcSteeringForce (in SteeringVelicle.cs)
	
	//CalcSteeringForce will get average direction of the flock from GameManager
	//Return the desired velocity
	public Vector3 Align(Vector3 direction){
		Vector3 desired = direction.normalized;
		desired *= attr.maxSpeed;
		desired = desired - body.velocity;
		return desired;
	}
	
	//CalcSteeringForce will get centroid from GameManager
	//Return the desired velocity
	public Vector3 Cohesion(Vector3 centroid){
		return Seek (centroid);
	}
	
	//CalcSteeringForce will get flockers from GameManager
	//Return the desired velocity
	public Vector3 Separation(float[] distancesToOtherFlockers, List<GameObject> flockers){
		Vector3 desired = Vector3.zero;
		
		int count = 0;
		
		for (int i = 0; i < distancesToOtherFlockers.Length; ++i){
			float distance = distancesToOtherFlockers[i];
			if (distance != 0){ //Don't do anything if its the same flocker
		        if (distance <= attr.separationDist){
					Vector3 fleeVector = transform.position - flockers[i].transform.position;
					fleeVector.Normalize();
					desired += fleeVector * (1/distance);
					++count;
				}
			}
		}
		
		if (count != 0){
		    desired /= count;
		}
		
		if(desired.sqrMagnitude != 0){
			desired.Normalize();
			desired *= attr.maxSpeed;
			desired = desired - body.velocity;
		}
		return desired;
	}
	
	private Vector3 getNormalPoint(WayPoint a, Vector3 prediction){
	    Vector3 ap = prediction - a.getPosition();
		
		Vector3 ab = a.getNormalizedVectorToNextWayPoint();
		ab *= Vector3.Dot (ap, ab);
		Vector3 normalPoint = a.getPosition() + ab;
		return normalPoint;
	}
	
	public Vector3 follow(Path path, WayPoint curWP){
        Vector3 predict = new Vector3 (body.velocity.x, body.velocity.y, body.velocity.z);
        predict.Normalize();
        predict *= 5; // Predict location 5 (arbitrary choice) frames ahead
        Vector3 predictLoc = body.transform.position + predict;

        // Now we must find the normal to the path from the predicted location
        // We look at the normal for each line segment and pick out the closest one
        Vector3 normal = Vector3.zero;
        Vector3 target = Vector3.zero;
        float worldRecord = float.MaxValue;  // Start with a very high worldRecord distance that can easily be beaten

		int first;
		int last;
		
		if (curWP == null){
		    first = 0;
		    last = path.getNumberOfWayPoints();
		}
		else{
			first = path.getNextWayPointIndex(curWP.ID);
			last = first + 3;
		}
		
	    for (int i = first; i < last; ++i) {
			//Look at a line segment
	        WayPoint a = path.getWayPointAtIndex(i % path.getNumberOfWayPoints());
			WayPoint b = path.getWayPointAtIndex((i + 1) % path.getNumberOfWayPoints());
	
	        // Get the normal point to that line
	        Vector3 normalPoint = getNormalPoint(a, predictLoc);
	
	        // Check if normal is on line segment
	        Vector3 dir = a.getVectorToNextWayPoint();
	        // If it's not within the line segment, consider the normal to just be the end of the line segment (point b)
	        //if (da + db > line.mag()+1) {
	        if (normalPoint.x < Mathf.Min(a.getPosition().x, b.getPosition().x) || 
				normalPoint.x > Mathf.Max(a.getPosition().x, b.getPosition().x) || 
				normalPoint.z < Mathf.Min(a.getPosition().z, b.getPosition().z) || 
				normalPoint.z > Mathf.Max(a.getPosition().z, b.getPosition().z)) {
	            
				normalPoint = new Vector3(b.getPosition().x, b.getPosition().y, b.getPosition().z);
				
	            // If we're at the end we really want the next line segment for looking ahead
	            a = path.getWayPointAtIndex(path.getNextWayPointIndex(a.ID));
	            b = path.getWayPointAtIndex(path.getNextWayPointIndex(b.ID));
	            dir = a.getVectorToNextWayPoint();
	        }
	
	        // How far away are we from the path?
	        float d = Vector3.Distance(predictLoc, normalPoint);
	        // Did we beat the worldRecord and find the closest line segment?
	        if (d < worldRecord) {
	            worldRecord = d;
	            normal = normalPoint;
	            curWP = path.getWayPointAtIndex(i % path.getNumberOfWayPoints());
	
	            // Look at the direction of the line segment so we can seek a little bit ahead of the normal
	            dir.Normalize();
	            // arbitrary.  It seems to work fine though
	            dir *= 25;
	            target = new Vector3(normal.x, normal.y, normal.z);
	            target += dir;
	        }
	    }
		
		Vector3 ret = Vector3.zero;
        // Only if the distance is greater than the path's radius do we bother to steer
        if (worldRecord > attr.pathRadius) {
            ret = Seek(target);
        }
		else if (body.velocity.magnitude <= (attr.maxSpeed * 0.3)){
			ret = Seek (target); //If a guy slows way down, we need to kick them into gear
		}
	    return ret;
	}
}
